<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Yaad Sarig Payment Gateway
 * Plugin Name: Woocommerce Yaad Sarig Payment Gateway
 * Description: Allows receiving payments via Yaad Sarig ( YaadPay )
 * Version: 1.1.2
 * Author: 10Bit
 * Author URI: http://www.10bit.co.il
 */
 
include '10bit_common.php';
include 'WC_Gateway_Yaadpay.php';
 
	
/**
 * Add the gateway to woocommerce
 */
function add_yaadpay_gateway( $methods ) {
	$methods[] = 'WC_Gateway_Yaadpay';
	return $methods;
}

add_filter( 'woocommerce_payment_gateways', 'add_yaadpay_gateway' );
	
function tenbit_gateway_yaad_add_settings_link( $links ) {
    $settings_link = '<a href="admin.php?page=wc-settings&tab=checkout&section=wc_gateway_yaadpay">'.__('Settings','10bit-woocommerce-gateway-yaadpay').'</a>';
  	array_push( $links, $settings_link );
  	return $links;
}

$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'tenbit_gateway_yaad_add_settings_link' );
?>