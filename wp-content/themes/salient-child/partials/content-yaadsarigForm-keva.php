<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 13/11/2015
 * Time: 01:38
 */

?>
<script type="text/javascript">
    jQuery(function ($) {
        var form = new validate($);
        form.init('#yaadSarig-keva');

    })

</script>
<form class="yaadSarig" id="yaadSarig-keva" style="text-align: right; width: 100%;" action="https://icom.yaad.net/cgi-bin/yaadpay/yaadpay.pl" method="post" target="_blank" name="YaadForm">
    <input name="Masof" type="hidden" value="4500186935"/>
    <input name="PassP" type="hidden" value="1234"/>
    <input name="action" type="hidden" value="pay"/>
    <input name="Order" type="hidden" value="1"/>
    <input name="Tash" type="hidden" value="999"/>
    <input name="SendHesh" type="hidden" value="True"/>
    <input name="UTF8" type="hidden" value="True"/>
    <input name="UTF8out" type="hidden" value="True"/>
    <input name="HK" type="hidden" value="True"/>
    <INPUT TYPE="hidden" value="OnlyOnApprove" NAME="True">
    <input name="SendHesh" type="hidden" value="True"/>

    <div style="width: 100%; border: solid 1px #888888; padding: 15px; margin: 15px 0px 15px 0px; border-radius: 4px;">
        <div id="shem" style="color: red; float: none;"></div>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>סכום התרומה:</label></div>
        <select style="width:100%; " NAME="Amount">
            <option value="20">20 ש"ח</option>
            <option value="50">50 ש"ח</option>
            <option value="70">70 ש"ח</option>
            <option value="100">100 ש"ח</option>
            <option value="150">150 ש"ח</option>
            <option value="">סכום אחר לבחירה בדף התשלום</option>
        </select>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>שם מלא:</label></div>
        <input class="ClientName" name="ClientName" type="text"/>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>טלפון נייד:</label></div>
        <input class="cell" name="cell" type="text"/>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>דוא"ל:</label></div>
        <input class="email" name="email" type="text"/>

        <input type="checkbox" class="takanon" name="takanon"> קראתי את <a href="http://animalpolice.org.il/%D7%AA%D7%A7%D7%A0%D7%95%D7%9F-%D7%94%D7%90%D7%AA%D7%A8/" title="תקנון האתר" target="_blank">תקנון האתר</a> ואני מסכים/ה לכל האמור בו.
        <div style="display: none;" class="msg"></div>
    </div>

    <INPUT TYPE="hidden" value="תרומה עבור משמר בעלי החיים בישראל" NAME="Info">


    <div style="margin-top: 5px;">
        <div style="text-align: right; margin: 15px 0px 15px 0px;">
<!--            <input style="width: 100%;" name="submit" type="submit" value="לתרומה לחץ כאן"/>-->
            <div class="submit">לתרומה לחץ כאן</div>
        </div>
    </div>

</form>
