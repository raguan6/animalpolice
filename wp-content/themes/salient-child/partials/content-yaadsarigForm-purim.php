<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 13/11/2015
 * Time: 01:38
 */

?>
<script type="text/javascript">
    jQuery(function ($) {
        var form = new validate($);
        form.init('#yaadSarig');

    })


    var validate = function ($) {

        var that = this;
        this.init = function (formSelector) {
            this.form = $(formSelector);

            jQuery('.submit', this.form).click(function () {
                    if (that.doValidate()) {
                        that.form.submit();
                    }
                }
            )
            jQuery('.takanon', that.form).click(function () {
                if (jQuery('.takanon', that.form).is(":checked")) {
                    jQuery('.msg', that.form).css('display', 'none');
                }
            })
        }

        this.doValidate = function () {
            var name = jQuery('.ClientName', that.form);
            var cell = jQuery('.cell', that.form);
            var email = jQuery('.email', that.form);
            var takanon = jQuery('.takanon', that.form);
            var msg = jQuery('.msg', that.form);


            cell.removeClass('error animated shake');
            name.removeClass('error animated shake');
            email.removeClass('error animated shake');
            msg.css('display', 'none');
            var flag = true;

            if (name.val() == "") {
                name.addClass('error animated shake');
                flag = false;
            }

            if (cell.val() == "") {
                cell.addClass('error animated shake');
                flag = false;
            }
            if ((email.val() == "") || (!validateEmail(email.val()))) {
                email.addClass('error animated shake');
                flag = false;
            }
            if (!takanon.is(":checked")) {
                msg.html('יש לאשר את תקנון האתר').fadeIn('fast')
                flag = false;
            }

            return flag;
        }

    }

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }
</script>
<form class="yaadSarig" id="yaadSarig" style="text-align: right; width: 100%;" action="https://icom.yaad.net/cgi-bin/yaadpay/yaadpay.pl" method="post" target="_blank" name="YaadForm">
    <input name="Masof" type="hidden" value="4500186935"/>
    <input name="PassP" type="hidden" value="1234"/>
    <input name="action" type="hidden" value="pay"/>
    <input name="Order" type="hidden" value="1"/>
    <input name="Tash" type="hidden" value="1"/>
    <input name="SendHesh" type="hidden" value="True"/>
    <input name="UTF8" type="hidden" value="True"/>
    <input name="UTF8out" type="hidden" value="True"/>
    <input name="MoreData" type="hidden" value="True"/>
    <!--<INPUT TYPE="hidden" value="" id="A3" NAME="Info">-->
    <div style="width: 100%; border: solid 1px #888888; padding: 15px; margin: 15px 0px 15px 0px; border-radius: 4px;">
        <div id="shem" style="color: red; float: none;"></div>

<input type="hidden" name="Amount" value="40">
<!--        <div style="width: 125px; float: right; padding-top: 4px;"><label>סכום התרומה:</label></div>-->
<!--        <select style="width:100%; " NAME="Amount">-->
<!--            <option value="50">50 ש"ח</option>-->
<!--            <option value="100">100 ש"ח</option>-->
<!--            <option value="150">150 ש"ח</option>-->
<!--            <option value="180">180 ש"ח</option>-->
<!--            <option value="300">300 ש"ח</option>-->
<!--            <option value="">סכום אחר לבחירה בדף התשלום</option>-->
<!--        </select>-->

        <div style="width: 125px; float: right; padding-top: 4px;"><label>שם מלא:</label></div>
        <input class="ClientName" name="ClientName" type="text"/>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>טלפון נייד:</label></div>
        <input class="cell" name="cell" type="text"/>

        <div style="width: 125px; float: right; padding-top: 4px;"><label>דוא"ל:</label></div>
        <input class="email" name="email" type="text"/>

        <input type="checkbox" class="takanon" name="takanon"> קראתי את <a href="http://animalpolice.org.il/%D7%AA%D7%A7%D7%A0%D7%95%D7%9F-%D7%94%D7%90%D7%AA%D7%A8/" title="תקנון האתר" target="_blank">תקנון האתר</a> ואני מסכים/ה לכל האמור בו.
        <div style="display: none;" class="msg"></div>
    </div>

    <INPUT TYPE="hidden" value="תשלום עבור מסיבת פורים" NAME="Info">

    <div style="margin-top: 5px;">
        <div style="text-align: right; margin: 15px 0px 15px 0px;">
            <!--            <input style="width: 100%;" name="submit" type="submit" value="לתרומה לחץ כאן"/>-->
            <div class="submit">המשך לתשלום</div>
        </div>
    </div>

</form>

<style>
    .email  {
        margin-bottom: 10px;
    }
    .takanon{
        width: 17px;
        height: 17px;
        float: right;
        margin-top: 7px;
        margin-left: 10px;
    }
    .yaadSarig select {
        font-family: danidinregular;
        font-size: 21px;
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.45) !Important;
    }

    .msg {
        color: #ff0000;
    }

    .submit {
        background-color: #333333;
        padding: 7px 11px;
        border: none;
        color: #fff;
        cursor: pointer;
        text-align: center;
        transition: all 0.2s linear 0s;
        -moz-transition: all 0.2s linear 0s;
        -webkit-transition: all 0.2s linear 0s;
        -o-transition: all 0.2s linear 0s;
        width: auto;
        font-size: 19px;
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        -o-border-radius: 2px;
    }

    .submit:hover {
        background-color: #2070d4;
    }

    input.error {
        /*background: url("http://www.wallplayer.tv/images/wallplayer_en/icon_error_white.gif") no-repeat 97% center;*/
        border-color: red;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.2), 0 0 10px rgba(255, 0, 0, 0.2);
        border: 1px solid red;
    }

    input.success {
        background: url("https://www.tafensw.edu.au/howex/stylesets/tafensw/icons/tick-icon.png") no-repeat 97% center;
        border-color: green;
        box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.2), 0 0 10px rgba(0, 255, 0, 0.2);
    }

    .animated {
        -webkit-animation-fill-mode: both;
        -moz-animation-fill-mode: both;
        -ms-animation-fill-mode: both;
        -o-animation-fill-mode: both;
        animation-fill-mode: both;
        -webkit-animation-duration: 1s;
        -moz-animation-duration: 1s;
        -ms-animation-duration: 1s;
        -o-animation-duration: 1s;
        animation-duration: 1s;
    }

    @-webkit-keyframes shake {
        0%, 100% {
            -webkit-transform: translateX(0);
        }
        10%, 30%, 50%, 70%, 90% {
            -webkit-transform: translateX(-10px);
        }
        20%, 40%, 60%, 80% {
            -webkit-transform: translateX(10px);
        }
    }

    @-moz-keyframes shake {
        0%, 100% {
            -moz-transform: translateX(0);
        }
        10%, 30%, 50%, 70%, 90% {
            -moz-transform: translateX(-10px);
        }
        20%, 40%, 60%, 80% {
            -moz-transform: translateX(10px);
        }
    }

    @-o-keyframes shake {
        0%, 100% {
            -o-transform: translateX(0);
        }
        10%, 30%, 50%, 70%, 90% {
            -o-transform: translateX(-10px);
        }
        20%, 40%, 60%, 80% {
            -o-transform: translateX(10px);
        }
    }

    @keyframes shake {
        0%, 100% {
            transform: translateX(0);
        }
        10%, 30%, 50%, 70%, 90% {
            transform: translateX(-10px);
        }
        20%, 40%, 60%, 80% {
            transform: translateX(10px);
        }
    }

    .shake {
        -webkit-animation-name: shake;
        -moz-animation-name: shake;
        -o-animation-name: shake;
        animation-name: shake;
    }

    /* JUST FOR AESTHETICS */
    

    :focus {
        outline: 0;
    }
</style>