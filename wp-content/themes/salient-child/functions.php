<?php
add_action('facebook_sdk', 'add_sdk');
function add_sdk()
{
    if (is_front_page() || is_page(305)) {
        ?>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
    <?php
    }
}

//Shortcode API images
function myImageLink()
{
    return get_option('home') . '/wp-content/uploads/';
}

add_shortcode('files', 'myImageLink');
// end shortcode images

//################### yaad Sarig shortCode ##############
add_shortcode('yaadsarigForm', 'YaadSarigForm');
function YaadSarigForm($att)
{
    if (isset($att['type'])) {
        return getPartial('yaadsarigForm-' . $att['type']);
    } else
        return getPartial('yaadsarigForm');

}
//#######################################################

//################### PayPal shortCode ##############
add_shortcode('paypal', 'paypal');
function paypal($att)
{
    if (isset($att['type'])) {
        return getPartial('paypal-' . $att['type']);
    } else
        return getPartial('paypal');

}
//#######################################################
function getPartial($name)
{
    ob_start();
    get_template_part('partials/content', $name);
    return ob_get_clean();
}

?>