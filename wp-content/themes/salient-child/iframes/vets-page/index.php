<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 12/08/2015
 * Time: 01:03
 */
?>
<!DOCTYPE html>
<html>
<head>
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="../tools/iframe-resizer-master/js/iframeResizer.contentWindow.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.min.css">
    <link rel="stylesheet" href="styles.css">
</head>
<body>
<div id="vets" ng-app="vets" ng-controller="vetsCtrl">
    <div class="contain">
        <div class="form-group form-group-lg">
            <input ng-model="searchText" type="text" class="form-control"  placeholder="חפש רופא">
        </div>
        <table class="table table-condensed table-hover">
            <thead>
            <tr class="active">
                <th >{{th.name}}</th>
                <th>{{th.city}}</th>
                <th>{{th.email}}</th>
                <th>{{th.phone}}</th>
                <th>{{th.fax}}</th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="vet in vets | filter:searchText" ng-class='{"info":vet.click %2 == 1 }' ng-click='vet.click=vet.click+1'>
                <td>{{vet.docName}}</td>
                <td>{{vet.localAuthority}}</td>
                <td>
                    <span class="glyphicon glyphicon glyphicon glyphicon-envelope " aria-hidden="true"></span>

                    {{vet.email}}</td>
                <td>


                    {{vet.phone}}
                    <span class="glyphicon glyphicon glyphicon-earphone " aria-hidden="true"></span>

                </td>
                <td>
                    {{vet.fax}}
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<script>
    var app = angular.module('vets', []);
    app.controller('vetsCtrl', function ($scope, vetsFac) {
        $scope.th = {
            name: 'שם רופא',
            city:'יישוב',
            email: 'אימייל',
            phone: 'טלפון',
            fax: 'פקס'
        }

        vetsFac.getVetsJson().then(function (res) {
            $scope.vets = res.data;
        })
    });

    app.factory('vetsFac', function ($http) {
        return {
            getVetsJson: function () {
                return $http.get('vetsJson.json')
                    .success(function (response) {
                        return response.data;
                    });
            }
        }

    })
</script>
</body>
</html>


