/**
 * Created by Rag-Home on 14/08/2015.
 */


var app = angular.module('complaint', ['angucomplete-alt', 'angularFileUpload', 'ngSanitize']);
app.controller('complaintCtrl', function ($scope, complaintFac, FileUploader, $sce) {

    $scope.data = {};
    $scope.loading = {};
    $scope.loading.showBox = false;
    $scope.addComplaint = function () {


        if (($scope.complaintForm.$valid ) && (!$scope.disableSubmit)) {
            $scope.loading.showBox = true;
            $scope.loading.status = 'loading';
            $scope.loading.statusText = '<span>שולח תלונה ...</span>';
            complaintFac.addComplaint($scope.data, {"action": 'step1'}).then(function (res) {
                $scope.recordResponse = res;
                if (res['success']) {
                    $scope.recordID = $scope.recordResponse['recordID'];
                    $scope.compaintID = $scope.recordResponse['complaintUserID'];
                    if (uploader.queue.length > 0) {
                        $scope.loading.statusText = '<span>מעלה תמונות ...</span>';
                        uploader.uploadAll();
                    }
                    else {
                        responseToClient('success');
                    }

                }
                else {
                    // record creation failed
                    responseToClient('fail');
                }
            })
        }
        else {
            // form validation failed
            //console.log('naaattttt valid');
        }
    }

    function responseToClient(status) {
        switch (status) {
            case 'success':
                $scope.loading.status = 'success';
                $scope.loading.statusText = '<span> תודה על פנייתך. אנו נטפל בה בהקדם. מספר תלונה: ' + $scope.compaintID + '</span>';
                if (($scope.data.city.placeCode != 8600) && ($scope.data.city.placeCode != 5000)) { // if we are not working in those areas
                    $scope.cssClass = 'redText';
                    $scope.loading.statusText = '<span>תודה על פנייתך. אנו לא פועלים באיזור- ' + $scope.data.city.name + '. שלחנו לך למייל הנחיות כיצד ניתן לפעול באיזורים בהם אנו לא פועלים. תודה ובהצלחה.</span>';
                }
                break;
            case 'fail':
                $scope.loading.status = 'fail';
                $scope.loading.statusText = '<span>אירעה שגיאה בשליחת התלונה. צור קשר עם משרדי העמותה.</span>';
                break;
        }

    }

    $scope.loading.safeResponse = function () {
        return $sce.trustAsHtml($scope.loading.statusText);
    }

    $scope.citySelected = function (selected) {
        if (selected) {
            $scope.data.city = selected.originalObject;
        }
    }
    $scope.places = [];
    complaintFac.getPlacesJson().then(function (res) {
        $scope.places = res.data;
    })

//----------------------------------------- File Uploader ----------------------------------------------
    var uploader = $scope.uploader = new FileUploader({
        url: 'functions.php',
        queueLimit: 10
    });

    // FILTERS
    uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    $scope.sizeLimit = 7340032;
    uploader.filters.push({
        'name': 'enforceMaxFileSize',
        'fn': function (item) {
            if (item.size > $scope.sizeLimit) {
                $scope.fileSizeAlert = 'גודל תמונה מקסימאלי הוא : ' + $scope.sizeLimit / 1024 / 1024 + 'MB';
                //$scope.fileSizeAlertShow = true;
            }
            return item.size <= $scope.sizeLimit; // 10 MiB to bytes
        }
    });


    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
        $scope.fileSizeAlertShow = true;
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        $scope.fileSizeAlertShow = false;
    };
    //uploader.onAfterAddingAll = function (addedFileItems) {
    //    console.info('onAfterAddingAll', addedFileItems);
    //};
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
        item.formData.push({recordID: $scope.recordID, action: 'step2'});

    };
    //uploader.onProgressItem = function (fileItem, progress) {
    //    console.info('onProgressItem', fileItem, progress);
    //};
    //uploader.onProgressAll = function (progress) {
    //    console.info('onProgressAll', progress);
    //};
    //uploader.onSuccessItem = function (fileItem, response, status, headers) {
    //    console.info('onSuccessItem', fileItem, response, status, headers);
    //};
    //uploader.onErrorItem = function (fileItem, response, status, headers) {
    //    console.info('onErrorItem', fileItem, response, status, headers);
    //};
    //uploader.onCancelItem = function (fileItem, response, status, headers) {
    //    console.info('onCancelItem', fileItem, response, status, headers);
    //};
    //uploader.onCompleteItem = function (fileItem, response, status, headers) {
    //    console.info('onCompleteItem', fileItem, response, status, headers);
    //};
    uploader.onCompleteAll = function () {
        //console.info('onCompleteAll');
        responseToClient('success');
    };

    console.info('uploader', uploader);
});

app.factory('complaintFac', function ($http) {
    return {
        getPlacesJson: function () {
            return $http.get('places.json')
                .success(function (response) {
                    return response.data;
                });
        },
        addComplaint: function (data, params) {
            return $http({
                method: 'POST',
                url: 'functions.php',
                params: params,
                data: data,
                headers: {'Content-Type': 'application/json'}
            }).error(function (data, status, headers, config) {
                return "error";
            }).then(function (res) {
                return res.data;
            });
        }
    }

})
//----------------------------------------- File Uploader Directive ----------------------------------------------
app.directive('ngThumb', ['$window', function ($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function (item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function (file) {
            var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function (scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({width: width, height: height});
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);
