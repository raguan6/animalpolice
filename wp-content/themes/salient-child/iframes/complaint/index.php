<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 12/08/2015
 * Time: 01:03
 */
?>
<!DOCTYPE html>
<html>
<head>
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="../tools/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <link rel="stylesheet" href="../tools/node_modules/angucomplete-alt/angucomplete-alt.css">
    <link rel="stylesheet" href="styles.css">


    <!-- Fix for old browsers -->
    <script src="http://nervgh.github.io/js/es5-shim.min.js"></script>
    <script src="http://nervgh.github.io/js/es5-sham.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
    <script src="../tools/angular-file-upload-master/examples/console-sham.min.js"></script>

    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="../tools/angular-file-upload-master/dist/angular-file-upload.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-sanitize.js"></script>
    <script src="../tools/iframe-resizer-master/js/iframeResizer.contentWindow.min.js"></script>
    <script src="main.js"></script>
</head>
<body>
<div id="complaint" ng-app="complaint" ng-controller="complaintCtrl">
    <div class="contain">
        <form name="complaintForm" novalidate>
            <div class="row">
                <select ng-model="data.kind" required title="סוג אירוע">
                    <option value="">סוג אירוע</option>
                    <option value="הריגת בע&quot;ח">הריגת בע&quot;ח</option>
                    <option value="התעללות בבע&quot;ח">התעללות בבע&quot;ח</option>
                    <option value="הרעלת בע&quot;ח">הרעלת בע&quot;ח</option>
                    <option value="נטישת בע&quot;ח">נטישת בע&quot;ח</option>
                    <option value="הזנחת בע&quot;ח">הזנחת בע&quot;ח</option>
                    <option value="אחר">אחר</option>
                </select>
            </div>

            <div class="row">
                <textarea ng-pattern="/^[^&]+$/" name="description" maxlength="1999" required placeholder="פרטי האירוע-נא לפרט" ng-model="data.description"></textarea>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" ng-model="data.suspect_name" maxlength="30" size="20" type="text" name="suspect_name" placeholder="שם החשוד"/>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" name="suspect_address" required ng-model="data.suspect_address" maxlength="40" size="20" type="text" placeholder="רחוב+מס' בית"/>
            </div>
            <div class="row">
                <div class="countryAutoComplete">
                    <angucomplete-alt text-searching="מחפש תוצאות" text-no-results="לא נמצאו תוצאות" field-required="true" field-required-class="cityRequired" match-class="highlight" id="citySelect" placeholder="חפש עיר/ישוב" pause="100"
                                      selected-object="citySelected" local-data="places" search-fields="name" title-field="name"
                                      minlength="1" input-class="form-control form-control-small country left required"/>

                </div>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" ng-model="data.informer_name" name="informer_name" maxlength="30" size="20" type="text" placeholder="שם המודיע"/>
            </div>
            <div class="row">
                <input required ng-model="data.informer_mobile" name="informer_mobile" maxlength="10"  minlength="0" size="20" ng-pattern="/^[0-9]{1,10}$/" type="text"
                       placeholder="טלפון [לצורך אימות פרטים-נשאר חסוי]"/>
            </div>
            <div class="row">
                <input ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-model="data.informer_email" name="informer_email" required maxlength="80" type="email" placeholder="דואר אלקטרוני"/>
            </div>

            <div class="row">
                <div ng-show="uploader.isHTML5">
                    <div nv-file-drop="" uploader="uploader">
                        <div nv-file-over="" uploader="uploader" over-class="another-file-over-class" class="well my-drop-zone">
                            ניתן לגרור תמונות לפה
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="fileUpload btn btn-primary">
                    <span>העלאת תמונות מהמחשב</span>
                    <input type="file" nv-file-select="" uploader="uploader" class="upload" multiple/>
                </div>
            </div>
            <div class="row" ng-show="uploader.queue.length">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="50%">Name</th>
                        <th ng-show="uploader.isHTML5">Size</th>
                        <th ng-show="uploader.isHTML5">Progress</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in uploader.queue">
                        <td id="thumbRag">
                            <strong>{{ item.file.name }}</strong>

                            <div ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 70 }"></div>
                        </td>
                        <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size/1024/1024|number:2 }} MB</td>
                        <td ng-show="uploader.isHTML5">
                            <div class="progress" style="margin-bottom: 0;">
                                <div class="progress-bar" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                            </div>
                        </td>
                        <td class="text-center">
                            <span ng-show="item.isSuccess"><i class="glyphicon glyphicon-ok"></i></span>
                            <span ng-show="item.isCancel"><i class="glyphicon glyphicon-ban-circle"></i></span>
                            <span ng-show="item.isError"><i class="glyphicon glyphicon-remove"></i></span>
                        </td>
                        <td nowrap>
                            <button ng-disabled="uploader.isUploading" type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                <span class="glyphicon glyphicon-trash"></span> Remove
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <button id="submitButton" ng-disabled="loading.status == 'loading' || loading.status == 'success' || disableSubmit" type="submit" ng-click="addComplaint()">שלח</button>
            </div>
            <div class="row">
                <div class="validationErrors">
                <span class="error pop_up"
                      ng-show="complaintForm.description.$error.pattern || complaintForm.suspect_name.$error.pattern || complaintForm.suspect_address.$error.pattern || complaintForm.informer_name.$error.pattern">השדות אינם יכולים להכיל &</span>
                    <span ng-show="complaintForm.informer_mobile.$error.pattern " class="error pop_up">בשדה הטלפון ניתן למלא רק ספרות</span>

                    <span ng-show="complaintForm.$submitted && complaintForm.$error.required " class="error pop_up">יש למלא את השדות המסומנים באדום.</span>
                    <span class="error pop_up" ng-show="complaintForm.$submitted && complaintForm.informer_email.$error.pattern">אימייל לא תקין</span>
                    <span class="error pop_up" ng-show="disableSubmit">לצערנו, אנו עדיין לא פועלים באזור זה אך עושים כל מאמץ להרחיב את הפעילות לכל הארץ. בינתיים, ניתן לפעול על פי ההוראות מימין. בהצלחה!</span>
                    <span class="error pop_up" ng-show="fileSizeAlertShow">{{fileSizeAlert}}</span>
                </div>
            </div>
            <div class="row">
                <div class="whileLoading" ng-show="loading.showBox">
                    <div class="imgIndicator">
                        <img ng-show="loading.status == 'loading'" src="img/loadingDog.GIF" class="loading-gif">
                        <img ng-show="loading.status == 'success'" src="img/success.png" class="success">
                        <img ng-show="loading.status == 'fail'" src="img/fail.png" class="fail">
                    </div>
                    <div class="statusText" ng-class="cssClass" ng-bind-html="loading.safeResponse()">
                    </div>
                    <div class="progressHolder" ng-show="uploader.isUploading">
                        <div class="progress" style="">
                            <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>


