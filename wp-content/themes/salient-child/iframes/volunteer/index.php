<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 12/08/2015
 * Time: 01:03
 */
?>
<!DOCTYPE html>
<html>
<head>
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="../tools/iframe-resizer-master/js/iframeResizer.contentWindow.min.js"></script>
    <script src="../tools/node_modules/angucomplete-alt/angucomplete-alt.js"></script>
    <link rel="stylesheet" href="../tools/node_modules/angucomplete-alt/angucomplete-alt.css">
    <link rel="stylesheet" href="styles.css">
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-sanitize.js"></script>
    <script src="main.js"></script>
</head>
<body>
<div id="volunteer" ng-app="volunteer" ng-controller="volunteerCtrl">
    <div class="contain">
        <form name="volunteerForm" novalidate>
            <div class="row">
                <select ng-model="data.kind" required title="סוג ההתנדבות">
                    <option value="">סוג ההתנדבות</option>
                    <option value="התנדבות בעמותה">התנדבות בעמותה</option>
                    <option value="התנדבות ביחידת האכיפה של העמותה">התנדבות ביחידת האכיפה של העמותה</option>
                    <option value="שרות לאומי">שרות לאומי</option>
                </select>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" required ng-model="data.first_name" maxlength="30"   type="text" name="first_name" placeholder="שם פרטי"/>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" name="last_name" required ng-model="data.last_name" maxlength="30"   type="text" placeholder="שם משפחה"/>
            </div>
            <div class="row">
                <input ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-model="data.email" name="email" required maxlength="80" type="email" placeholder="דואר אלקטרוני"/>
            </div>
            <div class="row">
                <div class="countryAutoComplete">
                    <angucomplete-alt text-searching="מחפש תוצאות" text-no-results="לא נמצאו תוצאות"  field-required="true" field-required-class="cityRequired" match-class="highlight" id="citySelect" placeholder="חפש עיר/ישוב" pause="100"
                                      selected-object="citySelected" local-data="places" search-fields="name" title-field="name"
                                      minlength="1" input-class="form-control form-control-small country left required"/>

                </div>
            </div>
            <div class="row">
                <input required ng-model="data.mobile" name="mobile" maxlength="10"   ng-pattern="/^[0-9]{1,10}$/" type="text" placeholder="טלפון"/>
            </div>


            <div class="row">
                <textarea ng-pattern="/^[^&]+$/" name="msg_content" maxlength="253" placeholder="תוכן ההודעה" ng-model="data.msg_content"></textarea>
            </div>
          
            
            <div class="row">
                <button id="submitButton" ng-disabled="loading.status == 'loading' || loading.status == 'success' " type="submit" ng-click="addVolunteer()">שלח</button>
            </div>
            <div class="row">
                <div class="validationErrors">
                <span class="error pop_up"
                      ng-show="volunteerForm.msg_content.$error.pattern || volunteerForm.first_name.$error.pattern || volunteerForm.last_name.$error.pattern || volunteerForm.informer_name.$error.pattern">השדות אינם יכולים להכיל &</span>
                    <span ng-show="volunteerForm.mobile.$error.pattern " class="error pop_up"> בשדה הטלפון ניתן למלא רק ספרות</span>

                    <span ng-show="volunteerForm.$submitted && volunteerForm.$error.required " class="error pop_up">יש למלא את השדות המסומנים באדום.</span>
                    <span class="error pop_up" ng-show="volunteerForm.$submitted && volunteerForm.email.$error.pattern">אימייל לא תקין</span>
                </div>
            </div>
            <div class="row">
                <div class="whileLoading" ng-show="loading.showBox">
                    <div class="imgIndicator">
                        <img ng-show="loading.status == 'loading'" src="img/loadingDog.GIF" class="loading-gif">
                        <img ng-show="loading.status == 'success'" src="img/success.png" class="success">
                        <img ng-show="loading.status == 'fail'" src="img/fail.png" class="fail">
                    </div>
                    <div class="statusText" ng-bind-html="loading.safeResponse()">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>


