/**
 * Created by Rag-Home on 14/08/2015.
 */


var app = angular.module('volunteer', ['angucomplete-alt', 'ngSanitize']);
app.controller('volunteerCtrl', function ($scope, volunteerFac, $sce) {

    $scope.data = {};
    $scope.loading = {};
    $scope.loading.showBox = false;
    $scope.addVolunteer = function () {


        if ($scope.volunteerForm.$valid) {
            $scope.loading.showBox = true;
            $scope.loading.status = 'loading';
            $scope.loading.statusText = '<span>שולח טופס ...</span>';
            volunteerFac.addVolunteer($scope.data, {"action": 'addVolunteer'}).then(function (res) {
                $scope.recordResponse = res;
                if (res['success']) {
                    $scope.recordID = $scope.recordResponse['recordID'];
                    $scope.volunteerID = $scope.recordResponse['volunteerUserID'];
                    responseToClient('success');
                }
                else {
                    // record creation failed
                    responseToClient('fail');
                }
            })
        }
        else {
            // form validation failed
            //console.log('naaattttt valid');
        }
    }

    function responseToClient(status) {
        switch (status) {
            case 'success':
                $scope.loading.status = 'success';
                $scope.loading.statusText = '<span> תודה על פנייתך, נחזור אליך בקרוב.<br/>  מספר המתנדב שלך הוא: ' + $scope.volunteerID + '</span>';
                break;
            case 'fail':
                $scope.loading.status = 'fail';
                $scope.loading.statusText = '<span>אירעה שגיאה בשליחת הטופס. צור קשר עם משרדי העמותה.</span>';
                break;
        }

    }

    $scope.loading.safeResponse = function () {
        return $sce.trustAsHtml($scope.loading.statusText);
    }

    $scope.citySelected = function (selected) {
        if (selected) {
            $scope.data.city = selected.originalObject
        }
    }
    $scope.places = [];
    volunteerFac.getPlacesJson().then(function (res) {
        $scope.places = res.data;
    })
})

app.factory('volunteerFac', function ($http) {
    return {
        getPlacesJson: function () {
            return $http.get('places.json')
                .success(function (response) {
                    return response.data;
                });
        },
        addVolunteer: function (data, params) {
            return $http({
                method: 'POST',
                url: 'functions.php',
                params: params,
                data: data,
                headers: {'Content-Type': 'application/json'}
            }).error(function (data, status, headers, config) {
                return "error";
            }).then(function (res) {
                return res.data;
            });
        }
    }

})