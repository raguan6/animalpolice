<?php

/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 12/08/2015
 * Time: 03:59
 */
//ini_set('display_errors', 1);
ini_set('soap.wsdl_cache_enabled', '1');
header('Content-Type: text/html; charset=utf-8');

define("logVolunteer", "/var/www/animalpolice.org.il/wp-content/themes/salient-child/iframes/volunteer/amcher.log");
define("logComplaint", "/var/www/animalpolice.org.il/wp-content/themes/salient-child/iframes/complaint/amcher.log");
define("logGeneral", "/var/www/animalpolice.org.il/wp-content/themes/salient-child/iframes/contactUS/amcher.log");

class salesForce
{
    private $mySforceConnection, $clientData;

    function  __construct()
    {
        define("USERNAME", "amir@animalpolice.org.il");
        define("PASSWORD", "ADF112358");
        define("SECURITY_TOKEN", "BmRQXxvUz3EvWwJZsFRkCFvvA");

        require_once('Force.com-Toolkit-for-PHP/soapclient/SforcePartnerClient.php');

        $this->mySforceConnection = new SforcePartnerClient();
        $this->mySforceConnection->createConnection("../tools/salesForce/partner.wsdl.xml");
        $this->mySforceConnection->login(USERNAME, PASSWORD . SECURITY_TOKEN);
    }
    public function contactUS($data)
    {
        $this->clientData = $data;
        $records = array();
        $records[0] = new SObject();
        $records[0]->fields = array(
            'email__c' => (isset($data->email))? $data->email: '',
            'phone__c' => (isset($data->mobile))? $data->mobile: '',
            'subject__c' => (isset($data->subject))? $data->subject: '',
            'contactUs_name__c' => (isset($data->name))? $data->name: '',
            'content_msg__c' => (isset($data->msg_content))? $data->msg_content: ''
        );
        $records[0]->type = 'ContactUs__c';
        $response = $this->mySforceConnection->create($records);    // create Record
        $pageResponse = array();
        if ($response[0]->success) {       // record added successfully
            $pageResponse['recordID'] = $response[0]->id;
            $pageResponse['success'] = true;
        } else {
            // handling create record errors - write to log file
            $log = 'Request Data:<br/>' . json_encode($records, JSON_UNESCAPED_UNICODE) . '<br/><br/>';
            $log .= 'response:<br/>' . json_encode($response, JSON_UNESCAPED_UNICODE) . '<br/><br/>--------------------------------------------------------------------------------------------------------------------------------------------------------------<br/><br/>';
            error_log($log, 3, logGeneral);
            $pageResponse['success'] = false;
        }
        return $pageResponse;
    }

    public function volunteer($data)
    {
        $this->clientData = $data;
        $records = array();
        $records[0] = new SObject();
        $records[0]->fields = array(
            'email__c' => $data->email,
            'phone__c' => $data->mobile,
            'content_msg__c' => (isset($data->msg_content))? $data->msg_content: '',
            'last_name__c' => $data->last_name,
            'first_name__c' => $data->first_name,
            'volunteer_type__c' => $data->kind,

            'placeName__c' => $data->city->name,
            'placeCode__c' => $data->city->placeCode,
            'napaID__c' => $data->city->napaID,
            'napaName__c' => $data->city->napaName,
            'bureauID__c' => $data->city->bureauID,
            'bureauName__c' => $data->city->bureauName,
            'councilID__c' => $data->city->councilID,
            'councilName__c' => $data->city->councilName,
        );
        $records[0]->type = 'volunteer__c';
        $response = $this->mySforceConnection->create($records);    // create Record
        $pageResponse = array();
        if ($response[0]->success) {       // record added successfully
            $pageResponse['recordID'] = $response[0]->id;
            $pageResponse['success'] = true;
            $response2 = $this->mySforceConnection->retrieve('Id,Name', 'volunteer__c', $response[0]->id);         // get record ID for the user - complaint ID
            $pageResponse['volunteerUserID'] = $response2[0]->fields->Name;
        } else {
            // handling create record errors - write to log file
            $log = 'Request Data:<br/>' . json_encode($records, JSON_UNESCAPED_UNICODE) . '<br/><br/>';
            $log .= 'response:<br/>' . json_encode($response, JSON_UNESCAPED_UNICODE) . '<br/><br/>--------------------------------------------------------------------------------------------------------------------------------------------------------------<br/><br/>';
            error_log($log, 3, logVolunteer);
            $pageResponse['success'] = false;
        }
        return $pageResponse;
    }


    public function complaint($data)
    {
        $this->clientData = $data;
        $records = array();
        $records[0] = new SObject();
        $records[0]->fields = array(
            'reportedby__c' => 'אתר העמותה',
            'type__c' => $data->kind,
            'complaintDescription__c' => $data->description,
            'suspectName__c' => (isset($data->suspect_name))? $data->suspect_name: '',
            'street__c' => $data->suspect_address,
            'informerPhone__c' => $data->informer_mobile,
            'informerEmail__c' => $data->informer_email,
            'placeName__c' => $data->city->name,
            'placeCode__c' => $data->city->placeCode,
            'napaID__c' => $data->city->napaID,
            'napaName__c' => $data->city->napaName,
            'bureauID__c' => $data->city->bureauID,
            'bureauName__c' => $data->city->bureauName,
            'councilID__c' => $data->city->councilID,
            'councilName__c' => $data->city->councilName,
            'informerName__c' => (isset($data->informer_name))? $data->informer_name: '',
        );
        $records[0]->type = 'complaint__c';
        $response = $this->mySforceConnection->create($records);    // create Record
        $pageResponse = array();
        if ($response[0]->success) {       // record added successfully
            $pageResponse['recordID'] = $response[0]->id;
            $pageResponse['success'] = true;
            $response2 = $this->mySforceConnection->retrieve('Id,Name', 'complaint__c', $response[0]->id);         // get record ID for the user - complaint ID
            $pageResponse['complaintUserID'] = $response2[0]->fields->Name;
        } else {
            // handling create record errors - write to log file
            $log = 'Request Data:<br/>' . json_encode($records, JSON_UNESCAPED_UNICODE) . '<br/><br/>';
            $log .= 'response:<br/>' . json_encode($response, JSON_UNESCAPED_UNICODE) . '<br/><br/>--------------------------------------------------------------------------------------------------------------------------------------------------------------<br/><br/>';
            error_log($log, 3, logComplaint);
            $pageResponse['success'] = false;
        }
        return $pageResponse;
    }

    public function addAttachments($path, $fileName, $recordID)
    {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        echo 'path: '.$path;
        $data = file_get_contents($path);
        $base64 = base64_encode($data);

        $records2 = array();

        $records2[0] = new SObject();
        $records2[0]->fields = array(
            'Body' => $base64,
            'ParentId' => $recordID,
            'Name' => $fileName
        );
        $records2[0]->type = 'Attachment';
        $response2 = $this->mySforceConnection->create($records2);

        print_r($response2);
    }

}