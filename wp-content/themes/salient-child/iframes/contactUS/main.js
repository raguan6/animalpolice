/**
 * Created by Rag-Home on 14/08/2015.
 */


var app = angular.module('contactUS', ['ngSanitize']);
app.controller('contactUsCtrl', function ($scope, contactUsFac, $sce) {

    $scope.data = {};
    $scope.loading = {};
    $scope.loading.showBox = false;
    $scope.addVolunteer = function () {


        if ($scope.contactUsForm.$valid) {
            $scope.loading.showBox = true;
            $scope.loading.status = 'loading';
            $scope.loading.statusText = '<span>שולח טופס ...</span>';
            contactUsFac.addContactUS($scope.data, {"action": 'addContactUS'}).then(function (res) {
                $scope.recordResponse = res;
                if (res['success']) {
                    $scope.recordID = $scope.recordResponse['recordID'];
                    $scope.contactUsID = $scope.recordResponse['contactUsID'];
                    responseToClient('success');
                }
                else {
                    // record creation failed
                    responseToClient('fail');
                }
            })
        }
        else {
            // form validation failed
            //console.log('naaattttt valid');
        }
    }

    function responseToClient(status) {
        switch (status) {
            case 'success':
                $scope.loading.status = 'success';
                $scope.loading.statusText = '<span> תודה על פנייתך, אנו נטפל בה בהקדם.</span>';
                break;
            case 'fail':
                $scope.loading.status = 'fail';
                $scope.loading.statusText = '<span>אירעה שגיאה בשליחת הטופס. צור קשר עם משרדי העמותה.</span>';
                break;
        }

    }

    $scope.loading.safeResponse = function () {
        return $sce.trustAsHtml($scope.loading.statusText);
    }


})

app.factory('contactUsFac', function ($http) {
    return {
        addContactUS: function (data, params) {
            return $http({
                method: 'POST',
                url: 'functions.php',
                params: params,
                data: data,
                headers: {'Content-Type': 'application/json'}
            }).error(function (data, status, headers, config) {
                return "error";
            }).then(function (res) {
                return res.data;
            });
        }
    }

})