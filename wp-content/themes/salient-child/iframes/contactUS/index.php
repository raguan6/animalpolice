<?php
/**
 * Created by PhpStorm.
 * User: Rag-Home
 * Date: 12/08/2015
 * Time: 01:03
 */
?>
<!DOCTYPE html>
<html>
<head>
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
    <script src="../tools/iframe-resizer-master/js/iframeResizer.contentWindow.min.js"></script>
    <link rel="stylesheet" href="styles.css">
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-sanitize.js"></script>
    <script src="main.js"></script>
</head>
<body>
<div id="contactUS" ng-app="contactUS" ng-controller="contactUsCtrl">
    <div class="contain">
        <form name="contactUsForm" novalidate>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" name="subject" required ng-model="data.subject" maxlength="40"   type="text" placeholder="נושא הפנייה"/>
            </div>
            <div class="row">
                <input ng-pattern="/^[^&]+$/" required ng-model="data.name" maxlength="40"   type="text" name="name" placeholder="שם"/>
            </div>
            <div class="row">
                <input ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-model="data.email" name="email" required maxlength="80" type="email" placeholder="דואר אלקטרוני"/>
            </div>
            <div class="row">
                <input  ng-model="data.mobile" name="mobile" maxlength="10"   ng-pattern="/^[0-9]{1,10}$/" type="text" placeholder="טלפון"/>
            </div>
            <div class="row">
                <textarea ng-pattern="/^[^&]+$/" name="msg_content" maxlength="1760" placeholder="תוכן ההודעה" ng-model="data.msg_content"></textarea>
            </div>
            <div class="row">
                <button id="submitButton" ng-disabled="loading.status == 'loading' || loading.status == 'success' " type="submit" ng-click="addVolunteer()">שלח</button>
            </div>
            <div class="row">
                <div class="validationErrors">
                <span class="error pop_up"
                      ng-show="contactUsForm.msg_content.$error.pattern || contactUsForm.name.$error.pattern || contactUsForm.subject.$error.pattern">השדות אינם יכולים להכיל &</span>
                    <span ng-show="contactUsForm.mobile.$error.pattern " class="error pop_up">בשדה הטלפון ניתן למלא רק ספרות</span>

                    <span ng-show="contactUsForm.$submitted && contactUsForm.$error.required " class="error pop_up">יש למלא את השדות המסומנים באדום.</span>
                    <span class="error pop_up" ng-show="contactUsForm.$submitted && contactUsForm.email.$error.pattern">אימייל לא תקין</span>
                </div>
            </div>
            <div class="row">
                <div class="whileLoading" ng-show="loading.showBox">
                    <div class="imgIndicator">
                        <img ng-show="loading.status == 'loading'" src="img/loadingDog.GIF" class="loading-gif">
                        <img ng-show="loading.status == 'success'" src="img/success.png" class="success">
                        <img ng-show="loading.status == 'fail'" src="img/fail.png" class="fail">
                    </div>
                    <div class="statusText" ng-bind-html="loading.safeResponse()">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>
</body>
</html>


